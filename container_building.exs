alias Buildah.{Cmd, Print, Zypp}

catatonit_cmd = "catatonit"

zypp_catatonit_on_container = fn (container, options) ->
    Zypp.packages_no_cache(container, [
        "catatonit"
    ], options)
    Cmd.run(
        container,
        ["printenv", "PATH"],
        into: IO.stream(:stdio, :line)
    )
    Cmd.run(
        container,
        ["rpm", "--query", "--list", "--verbose", "catatonit"],
        into: IO.stream(:stdio, :line)
    )
    Cmd.run(
        container,
        ["/bin/sh", "-c", "command -v " <> catatonit_cmd],
        into: IO.stream(:stdio, :line)
    )
    {catatonit_exec_, 0} = Cmd.run(
        container,
        ["/bin/sh", "-c", "command -v " <> catatonit_cmd]
    )
    {_, 0} = Cmd.config(
        container,
        entrypoint: "[\"#{String.trim(catatonit_exec_)}\", \"--\"]"
    )
end

zypp_catatonit_test = fn (container, image_ID, options) ->
    {_, 0} = Cmd.run(container, [catatonit_cmd, "--version"], into: IO.stream(:stdio, :line))
    # {_, 0} = Podman.Cmd.run(image_ID, [catatonit_cmd, "--version"],
    #     tty: true, rm: true, into: IO.stream(:stdio, :line)
    # )
    # Error: error configuring network namespace for container b686a3cd899dc49bee35c9cd0400bffe6348642147d3c084d5e19a315513ccbb: Missing CNI default network
    Zypp.packages_no_cache(container, ["psmisc"], options)
    {_, 0} = Cmd.run(container, ["pstree"], into: IO.stream(:stdio, :line))
    # {_, 0} = Podman.Cmd.run(image_ID, ["pstree"],
    #     tty: true, rm: true, into: IO.stream(:stdio, :line)
    # )
end




{whoami_, 0} = System.cmd("whoami", [])
"root" = String.trim(whoami_)
{_, 0} = Cmd.version(into: IO.stream(:stdio, :line))
{_, 0} = Cmd.info(into: IO.stream(:stdio, :line))

# build_image = "opensuse"
# _image = "localhost/" <> build_image

# from_image = "docker.io/" <> build_image
from_image = System.fetch_env!("FROM_IMAGE")
IO.puts(from_image)

# image = "localhost/" <> build_image
# image = System.fetch_env!("IMAGE")
# IO.puts(image)

# destination = "docker://#{System.fetch_env!("CI_REGISTRY_IMAGE")}/" <> build_image
destination = System.fetch_env!("IMAGE_DESTINATION")
IO.puts(destination)

Buildah.from_push(
    from_image,
    zypp_catatonit_on_container,
    zypp_catatonit_test,
    destination: destination,
    quiet: System.get_env("QUIET")
)

Print.images()

